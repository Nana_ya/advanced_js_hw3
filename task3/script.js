const user1 = {
    name: "John",
    years: 30
};

let {name, years, isAdmin = false} = user1;
console.log("Name: " + name);
console.log("Years: " + years);
console.log("IsAdmin: " + isAdmin);
